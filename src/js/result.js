var re=new Vue({
    el:"#app",
    data:{
        conste:"Add your Constellation",
        consts:[
            "Add your Constellation","Aries ","Taurus","Gemini","Cancer","Leo","Virgo","Libra","Scorpio","Sagittarius","Capricorn","Aquarius","Pisces"
        ],
        curName: "",
        img_1:"/src/images/add_img_1.png",
        img_2:"/src/images/add_img_2.png",
        img_3:"/src/images/add_img_3.png",
        img_4:"/src/images/add_img_4.png",
        img_5:"/src/images/add_img_5.png",
        img_6:"/src/images/add_img_6.png",
        applyState: 2,
        uploadPercent: 0,
        uploadingFlag: 0,
        warm_alert_text:"",
        text_change:"",
        submit_src: "/src/images/submit_deflaut.png",
        z_styles:{
            z:true,
            class_b:false
        },
        alert_styles:{
            alert:true,
            alert_b:false
        },
        warm_alert_style:{
            warm_alert:true,
            warm_alert_b:false
        },
        isStateShow: 0,
        congra_style:{
            congra:true,
            congra_b:false
        },
        title:"",
        input_arr:{about_me:"",full_name:"",email:"",phone_number:"",birthdate:"",industryModel:""},
        about_me:"",
        full_name:"",
        email:"",
        phone_number:"",
        url: "",
        birthdate:"",
        warm:"Please add at least two pictures!<br />Format:jpg,jpeg,gif,png and bmp / Max Size: 10 MB",
        hometown:"Add your hometown",
        hometowns:[
            "Add your hometown", "Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Jharkhand","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Sikkim","Tripura","West Bengal","Delhi","Haryana","Himachal Pradesh","Jammu & Kashmir","Punjab","Rajasthan","Uttar Pradesh","Uttarakhand","Andhra Pradesh","Karnataka","Kerala","Tamil Nadu","Telangana","Goa","Gujarat","Madhya Pradesh","Maharashtra"
        ],
        personality:"Add your personality",
        personalitys:[
            "Add your personality",  "Charming","Active","Athletic","Creative","Friendly","Independent","Kind","Passionate",
            "Popular","Romantic","Sexy","Sweet","Modern","Passionate","Loveable","Intelligent"
        ],
        industryModel: "Add your industry",
        industrys:[
            "Add your industry","Student","Art","Entertainment","Finance & economic","Healthcare & Medicine","Industrial & Manufacturing","IT & Internet","Media","Retail","Science & Technology","Other"
        ]
    },

    watch:{

        img_1:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            load2();
            if ( this.about_me!==""&& this.full_name!==""&&this.email!==""&&this.phone_number!==""&&this.birthdate!==""&& this.industryModel!==""&&this.about_me!==null&& this.full_name!==null&&this.email!==null&&this.phone_number!==null&&this.birthdate!==null&&this.img_1!=="/src/images/add_img_1.png"&&this.img_2!=="/src/images/add_img_2.png"){
                this.submit_src="/src/images/submit.png";
            } else {
                this.submit_src="/src/images/submit_deflaut.png";
            }
        }},
        img_2:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
                load3();
            if ( this.about_me!==""&& this.full_name!==""&&this.email!==""&&this.phone_number!==""&&this.birthdate!==""&& this.industryModel!==""&&this.about_me!==null&& this.full_name!==null&&this.email!==null&&this.phone_number!==null&&this.birthdate!==null&&this.img_1!=="/src/images/add_img_1.png"&&this.img_2!=="/src/images/add_img_2.png"){
                this.submit_src="/src/images/submit.png";
            } else {
                this.submit_src="/src/images/submit_deflaut.png";
            }
        } },
        img_3:function () {
            if(this.applyState!=="1"&&this.applyState!=="4") {
                load4();
            }},
        img_4:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){ load5();
        }},
        img_5:function () {
            if(this.applyState!=="1"&&this.applyState!=="4") {
                load6();
            }},
        about_me:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.about_me==""|this.about_me==null){
                document.getElementById('full_name_id').disabled=true;
                document.getElementById('email_id').disabled=true;
                document.getElementById('phone_number_id').disabled=true;
                document.getElementById('birthdate_id').disabled=true;
                document.getElementById('industry_id').disabled=true;
                document.getElementById('hometown_id').disabled=true;
                document.getElementById('personality_id').disabled=true;
            }else {
                document.getElementById("full_name_id").disabled=false;

            }
        }else {
                document.getElementById('about_me_id').disabled=true;
            }
        },
        full_name:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.full_name==""|this.full_name==null){
                document.getElementById('email_id').disabled=true;
                document.getElementById('phone_number_id').disabled=true;
                document.getElementById('birthdate_id').disabled=true;
                document.getElementById('industry_id').disabled=true;
                document.getElementById('hometown_id').disabled=true;
                document.getElementById('personality_id').disabled=true;
            } else {
                document.getElementById('email_id').disabled=false;
            }
        }},
        email:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.email==""|this.email==null){
                document.getElementById('phone_number_id').disabled=true;
                document.getElementById('birthdate_id').disabled=true;
                document.getElementById('industry_id').disabled=true;
                document.getElementById('hometown_id').disabled=true;
                document.getElementById('personality_id').disabled=true;
            }else {
                document.getElementById('phone_number_id').disabled=false;
            }
        }},
        phone_number:function () {
            if(this.applyState!=="1"&&this.applyState!=="4") {
                if (this.phone_number == "" | this.phone_number == null) {
                    document.getElementById('birthdate_id').disabled = true;
                    document.getElementById('industry_id').disabled = true;
                    document.getElementById('hometown_id').disabled = true;
                    document.getElementById('personality_id').disabled = true;
                } else {
                    document.getElementById('birthdate_id').disabled = false;
                }
            }},
        birthdate:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            this.input_arr['birthdate']=this.birthdate;
            if(this.birthdate==""|this.birthdate==null){
                document.getElementById('industry_id').disabled=true;
                document.getElementById('hometown_id').disabled=true;
                document.getElementById('personality_id').disabled=true;
            }else {
                document.getElementById('industry_id').disabled=false;
                document.getElementById('industry2_id').disabled=false;
                this.industrys=[
                    "Add your industry","Student","Art","Entertainment","Finance & economic","Healthcare & Medicine","Industrial & Manufacturing","IT & Internet","Media","Retail","Science & Technology","Other"
                ];
            }
        }},
        industryModel:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.industryModel==""|this.industryModel==null){
                document.getElementById('hometown_id').disabled=true;
                document.getElementById('personality_id').disabled=true;
            }  else {
                this.hometowns=[
                    "Add your hometown", "Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Jharkhand","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Sikkim","Tripura","West Bengal","Delhi","Haryana","Himachal Pradesh","Jammu & Kashmir","Punjab","Rajasthan","Uttar Pradesh","Uttarakhand","Andhra Pradesh","Karnataka","Kerala","Tamil Nadu","Telangana","Goa","Gujarat","Madhya Pradesh","Maharashtra"
                ];
                this.personalitys=[
                    "Add your personality",  "Charming","Active","Athletic","Creative","Friendly","Independent","Kind","Passionate",
                    "Popular","Romantic","Sexy","Sweet","Modern","Passionate","Loveable","Intelligent"
                ];
                document.getElementById('hometown_id').disabled=false;
                document.getElementById('personality_id').disabled=false;
                document.getElementById('constellation_id').disabled=false;
            }
        }},
        hometown:function () {
            if(this.applyState!=="1"&&this.applyState!=="4") {
                if (this.hometown == "" | this.hometown == null) {
                    document.getElementById('personality_id').disabled = true;
                } else {
                    document.getElementById('personality_id').disabled = false;
                }
            }},
        input_arr:{
            handler:function () {
                if ( this.about_me!==""&& this.full_name!==""&&this.email!==""&&this.phone_number!==""&&this.birthdate!==""&& this.industryModel!=="Add your industry"&& this.industryModel!==""&&this.about_me!==null&& this.full_name!==null&&this.email!==null&&this.phone_number!==null&&this.birthdate!==null&&this.img_1!=="/src/images/add_img_1.png"&&this.img_2!=="/src/images/add_img_2.png"){
                    this.submit_src="/src/images/submit.png";
                } else {
                    this.submit_src="/src/images/submit_deflaut.png";
                }
            },
            deep:true
        },
        title:function () {
            if(this.title=='Abuout Me'){
                this.text_change="";
            }else if(this.title=='Full Name'){
                this.text_change="";
            }else if(this.title=='Email'){
                this.text_change="";
            }else if(this.title=='Phone Number'){
                this.text_change="";
            }
        }
    },
    computed:{
        z_style:function () {
            return{
                display:"none"
            }
        }
    },
    methods:{
        aaa:function (event) {
            var ids= event.target.id;
            alert(ids)
        },
        imgPath_f:function (event) {
            this.curName=event.target.name;
        },
        aboutme_alert_f:function () {
            this.text_change=this.about_me;
            gtag('event', "click", {
                event_category: "result",
                event_label: "me"
            });
            document.querySelector('#on_input textarea').select()
        },
        fullname_alert_f:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.about_me!==""&&this.about_me!==null){
                this.text_change=this.full_name;
                gtag('event', "click", {
                    event_category: "result",
                    event_label: "name"
                });
                document.getElementById('full_name_id').disabled=false;
                document.querySelector('#on_input textarea').select()
            }
        }},
        email_f:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.about_me!==''&&this.about_me!==null&&this.full_name!==""&&this.full_name!==null){
                this.text_change=this.email;
                gtag('event', "click", {
                    event_category: "result",
                    event_label: "email"
                });
                document.getElementById("email_id").disabled=false;
                document.querySelector('#on_input textarea').select()
            }
        }},
        phone_number_f:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.about_me!==''&&this.about_me!==null&&this.full_name!==""&&this.full_name!==null&&this.email!==""&&this.email!==null){
                this.text_change=this.phone_number;
                gtag('event', "click", {
                    event_category: "result",
                    event_label: "phone"
                });
                document.getElementById("phone_number_id").disabled=false;
                document.querySelector('#on_input textarea').select()
            }
        }},
        birthdate_alert_f:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.about_me!==''&&this.about_me!==null&&this.full_name!==""&&this.full_name!==null&&this.email!==""&&this.email!==null&&this.phone_number!==""&&this.phone_number!==null){
                this.text_change=this.birthdate;
                gtag('event', "click", {
                    event_category: "result",
                    event_label: "birthdate"
                });
                document.getElementById("birthdate_id").disabled=false
            }
        }},
        industry_f:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.about_me!==''&&this.about_me!==null&&this.full_name!==""&&this.full_name!==null&&this.email!==""&&this.email!==null&&this.phone_number!==""&&this.phone_number!==null&&this.birthdate!==""&&this.birthdate!==null){
                document.getElementById("industry_id").disabled=false;
                gtag('event', "click", {
                    event_category: "result",
                    event_label: "industry"
                });
            }
        }},
        hometown_f:function () {
            if(this.applyState!=="1"&&this.applyState!=="4"){
            if(this.about_me!==''&&this.about_me!==null&&this.full_name!==""&&this.full_name!==null&&this.email!==""&&this.email!==null&&this.phone_number!==""&&this.phone_number!==null&&this.birthdate!==""&&this.birthdate!==null&&this.industryModel!==""&&this.industryModel!==null){
                document.getElementById("hometown_id").disabled=false;
                document.getElementById("personality_id").disabled=false;
                gtag('event', "click", {
                    event_category: "result",
                    event_label: "hometown"
                });
            }
        }},
        industryModel_f:function (event) {
            this.input_arr['industryModel']=this.industryModel=event.target.value;
        },
        hometownModel_f:function (event) {
            this.hometown=event.target.value
        },
        personalityModel_f:function (event) {
            gtag('event', "click", {
                event_category: "result",
                event_label: "personality"
            });
            this.personality=event.target.value
        },
        warm_alert_f:function () {
            this.warm_alert_style.warm_alert=true;
            this.warm_alert_style.warm_alert_b=false;
        },
        summit_f:function () {
            var src= this.submit_src;
            if(src=="/src/images/submit.png"&&this.img_1!=="/src/images/add_img_2.png"&&this.img_2!=="/src/images/add_img_2.png"){

                this.summit();
            }else{}
        },
        summit:function () {

            this.$http.post('/index.php?c=index&f=submit_data',
                {data:{
                    image: {
                        img_1: this.img_1,
                        img_2: this.img_2,
                        img_3: this.img_3,
                        img_4: this.img_4,
                        img_5: this.img_5,
                        img_6: this.img_6
                    },
                    constellation:this.conste,
                    about:this.about_me,
                    name:this.full_name,
                    email:this.email,
                    phone:this.phone_number,
                    birthdate:this.birthdate,
                    hometown:this.hometown,
                    personality:this.personality,
                    industry:this.industryModel}},
                {
                emulateJSON: true
            }).then(function (res) {
                data = JSON.parse(res.body)
                if (data.code == "200") {
                    this.congra_style.congra_b=true;
                    this.congra_style.congra=false;
                    this.z_styles.z=false;
                    this.z_styles.class_b=true;
                } else {
                    alert(data.message)
                }

            })

            gtag('event', "click", {
                event_category: "result",
                event_label: "submit"
            });
        },
        alert_f:function (event) {
            var el=event.currentTarget;
            var title_name=$(el).attr("name");
            this.title=title_name;
            model_names=title_name.replace(/\s+/,"_").toLowerCase();
            this.z_styles.z=false;
            this.z_styles.class_b=true;
            this.alert_styles.alert=false;
            this.alert_styles.alert_b=true;
        },
        birthdate_f:function (event) {
            var date=event.target.valueOf;
        },

        done_f:function (event) {
            gtag('event', "click", {
                event_category: "result",
                event_label: "done"
            });
            // var el=event.currentTarget;
            var title_name=this.title;
            if(title_name=="Abuout Me"){
                var reg=/^[\s\S]{1,400}$/;
                var re=new RegExp(reg);
                var abuout_me=this.text_change;
                if(re.test(abuout_me)){
                    this.done_close();
                    this.about_me=this.input_arr['about_me']=this.text_change;
                }else {
                    gtag('event', "error", {
                        event_category: "result",
                        event_label: "me"
                    });
                    this.warm_alert_style.warm_alert=false;
                    this.warm_alert_style.warm_alert_b=true;
                    this.warm_alert_text="Description may only contain 400 characters at most."
                }
            }else if(title_name=="Full Name"){
                var reg_a=/^[a-zA-Z\._-][\sa-zA-Z\._-]+$/;
                var reg_b=/^[a-zA-Z\._-][\sa-zA-Z\._-]{1,100}$/;
                var re_a=new RegExp(reg_a);
                var re_b=new RegExp(reg_b);
                var full_name=this.text_change;
                if(!re_a.test(full_name)){
                    gtag('event', "error", {
                        event_category: "result",
                        event_label: "name"
                    });
                    this.warm_alert_style.warm_alert=false;
                    this.warm_alert_style.warm_alert_b=true;
                    this.warm_alert_text="Full Name may only contain alphabets."
                }else {
                    if(re_b.test(full_name)){
                        this.done_close();
                        this.full_name=this.input_arr['full_name']=this.text_change;

                    }else {
                        this.warm_alert_style.warm_alert=false;
                        this.warm_alert_style.warm_alert_b=true;
                        this.warm_alert_text="Full Name may only contain 100 characters at most."
                    }
                }
            }else if(title_name=="Email"){
                var reg=/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.com$/;
                var re=new RegExp(reg);
                var email=this.text_change;
                if(re.test(email)){
                    this.done_close();
                    this.email=this.input_arr['email']=this.text_change;
                }else {
                    gtag('event', "error", {
                        event_category: "result",
                        event_label: "email"
                    });
                    this.warm_alert_style.warm_alert=false;
                    this.warm_alert_style.warm_alert_b=true;
                    this.warm_alert_text="Please enter a valid Email Address."
                }
            }else if(title_name=="Phone Number"){
                var reg_a=/^[0-9]+$/;
                var reg_b=/^[0-9]{10,}$/;
                var re_a=new RegExp(reg_a);
                var re_b=new RegExp(reg_b);
                var phone_number=this.text_change;
                if(!re_a.test(phone_number)){
                    gtag('event', "error", {
                        event_category: "result",
                        event_label: "phone"
                    });
                    this.warm_alert_style.warm_alert=false;
                    this.warm_alert_style.warm_alert_b=true;
                    this.warm_alert_text="Phone Number may only contain digits."
                }else {
                    if(re_b.test(phone_number)){
                        this.done_close();
                        this.phone_number=this.input_arr['phone_number']=this.text_change;
                    }else {
                        this.warm_alert_style.warm_alert=false;
                        this.warm_alert_style.warm_alert_b=true;
                        this.warm_alert_text="Phone Number should be at least 10 digits."
                    }
                }
            }
        },

        voteFunc: function() {
            window.location.href = this.url
        },
        done_close:function(){

            this.z_styles.z=true;
            this.z_styles.class_b=false;
            this.alert_styles.alert=true;
            this.alert_styles.alert_b=false;
        },
        cancel_f:function () {
            this.z_styles.z=true;
            this.z_styles.class_b=false;
            this.alert_styles.alert=true;
            this.alert_styles.alert_b=false;
        },
        z_f:function () {
            this.z_styles.z=true;
            this.z_styles.class_b=false;
            this.alert_styles.alert=true;
            this.alert_styles.alert_b=false;
        },
        congra_f:function () {
            this.congra_style.congra=true;
            this.congra_style.congra_b=false;
            this.z_styles.z=true;
            this.z_styles.class_b=false;
            location.reload();
        },

        getData: function() {
            this.$http.post(
                    "/index.php?c=index&f=get_data",
                    {
                        emulateJSON: true
                    }
            ).then(function(res) {

                if (JSON.parse(res['data'])['code'] == "200") {
                    var data = JSON.parse(res['data'])['data'];
                    this.about_me=data.about;
                    this.full_name=data.name;
                    this.email=data.email;
                    this.phone_number=data.phone;
                    this.birthdate=data.birthday;
                    this.applyState = data.status;
                    this.url = data.url
                    if(data.status=="3"){
                        this.industryModel=data.industry;
                        this.hometown=data.hometown;
                        this.personality=data.personlity;
                        this.conste=data.constellation;
                    }
                    if(data.status=="1"||data.status=="4"){
                        this.industryModel=data.industry;
                        this.hometown=data.hometown;
                        this.personality=data.personlity;
                        this.conste=data.constellation;
                        document.getElementById('full_name_id').disabled=true;
                        document.getElementById('email_id').disabled=true;
                        document.getElementById('phone_number_id').disabled=true;
                        document.getElementById('birthdate_id').disabled=true;
                        document.getElementById('industry_id').disabled=true;
                        document.getElementById('hometown_id').disabled=true;
                        document.getElementById('personality_id').disabled=true;
                        document.getElementById('constellation_id').disabled=true;
                        this.submit_src="/src/images/submit_deflaut.png";

                    }else {load1();}

                    //初始化图片
                    if (JSON.parse(data.image)) {
                        var imageTmp = JSON.parse(data.image);

                        if (!this.img_1) {
                            this.img_1 = "/src/images/add_img_1.png"
                        } else {
                            this.img_1 = imageTmp['img_1']
                        }

                        if (!this.img_2) {
                            this.img_2 = "/src/images/add_img_2.png"
                        } else {
                            this.img_2 = imageTmp['img_2']
                        }


                        if (!this.img_3) {
                            this.img_3 = "/src/images/add_img_3.png"
                        } else {
                            this.img_3 = imageTmp['img_3']
                        }


                        if (!this.img_4) {
                            this.img_4 = "/src/images/add_img_4.png"
                        } else {
                            this.img_4 = imageTmp['img_4']
                        }


                        if (!this.img_5) {
                            this.img_5 = "/src/images/add_img_5.png"
                        } else {
                            this.img_5 = imageTmp['img_5']
                        }

                        if (!this.img_6) {
                            this.img_6 = "/src/images/add_img_6" +
                                    ".png"
                        } else {
                            this.img_6 = imageTmp['img_6']
                        }
                    }
                } else {load1();}
            })
        }
    },
    mounted: function() {
        this.getData()
    }
});

function pluploadError(uploader, errObject) {

    console.log(errObject)
    if (errObject.code == -601) {
        re.isStateShow=1;
        document.getElementById("state_tip").innerHTML="Format Error！Only jpg,jpeg,gif,png and bmp format are allowed";
        setTimeout(function () {
            re.isStateShow=0;
        },3000)
    } else if (errObject.code == -600) {
        re.isStateShow=1;
        document.getElementById("state_tip").innerHTML="Size Error！" +
                "Picture should be more than 100KB, less than 10MB";
        setTimeout(function () {
            re.isStateShow=0;
        },3000)
    }
}

function load1() {
    var uploader = new plupload.Uploader({
        runtimes: 'html5, html4',
        browse_button: 'person1', //触发文件选择对话框的按钮，为那个元素id
        url: '/index.php?c=index&f=upload', //服务器端的上传页面地址
        flash_swf_url: '/src/js/plupload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        filters: {
            mime_types: [
                {title: "Image files", extensions: "jpg,gif,png,bmp"}
            ],
            max_file_size: '10mb'
        }, //图片限制
        silverlight_xap_url: 'js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        init: {
            FilesAdded: function (uploader, files) {
                uploader.start()
            }
        }
    });

    uploader.init();
    uploader.bind('FilesAdded', function (uploader, files) {
        uploader.start();
    });

    uploader.bind('Error', pluploadError);

    uploader.bind('UploadProgress', function (uploader, file) {
        console.log(file.percent)
        re.uploadingFlag = 1
        re.uploadPercent = file.percent
    })

    uploader.bind('FileUploaded', function (uploader, file, responseObject) {
        var data = JSON.parse(responseObject.response);
        re.uploadingFlag = 0

        if (data && data['code'] == "200") {
            var imagePath = "/" + data['data'];
            document.getElementById('person1').src = re.img_1= imagePath;
            var width=document.getElementById("person1").style.width;

            gtag('event', "click", {
                event_category: "result",
                event_label: "addphoto"
            });

            gtag('event', "click", {
                event_category: "result",
                event_label: "add1"
            });
        }
    })

}
function load2() {
    var uploader = new plupload.Uploader({
        runtimes: 'html5, html4',
        browse_button: 'person2', //触发文件选择对话框的按钮，为那个元素id
        url: '/index.php?c=index&f=upload', //服务器端的上传页面地址
        flash_swf_url: '/src/js/plupload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        filters: {
            mime_types: [
                {title: "Image files", extensions: "jpg,gif,png,bmp"}
            ],
            max_file_size: '10mb'
        }, //图片限制
        silverlight_xap_url: 'js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        init: {
            FilesAdded: function (uploader, files) {
                uploader.start()
            }
        }
    });

    uploader.init();
    uploader.bind('FilesAdded', function (uploader, files) {
        uploader.start();
    });

    uploader.bind('Error', pluploadError);
    uploader.bind('UploadProgress', function (uploader, file) {
        console.log(file.percent)
        re.uploadingFlag = 1
        re.uploadPercent = file.percent
    })

    uploader.bind('FileUploaded', function (uploader, file, responseObject) {
        var data = JSON.parse(responseObject.response);
        re.uploadingFlag = 0


        if (data && data['code'] == "200") {
            var imagePath = "/" + data['data'];
            document.getElementById('about_me_id').disabled=false;
            document.getElementById('person2').src = re.img_2 = imagePath;
            gtag('event', "click", {
                event_category: "result",
                event_label: "addphoto"
            });

            gtag('event', "click", {
                event_category: "result",
                event_label: "add2"
            });
        }
    })

}
function load3() {
    var uploader = new plupload.Uploader({
        runtimes: 'html5, html4',
        browse_button: 'person3', //触发文件选择对话框的按钮，为那个元素id
        url: '/index.php?c=index&f=upload', //服务器端的上传页面地址
        flash_swf_url: '/src/js/plupload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        filters: {
            mime_types: [
                {title: "Image files", extensions: "jpg,gif,png,bmp"}
            ],
            max_file_size: '10mb'
        }, //图片限制
        silverlight_xap_url: 'js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        init: {
            FilesAdded: function (uploader, files) {
                uploader.start()
            }
        }
    });

    uploader.init();
    uploader.bind('FilesAdded', function (uploader, files) {
        uploader.start();
    });
    uploader.bind('Error', pluploadError);    uploader.bind('UploadProgress', function (uploader, file) {
        console.log(file.percent)
        re.uploadingFlag = 1
        re.uploadPercent = file.percent
    })

    uploader.bind('FileUploaded', function (uploader, file, responseObject) {
        var data = JSON.parse(responseObject.response);
        re.uploadingFlag = 0

        if (data && data['code'] == "200") {
            var imagePath = "/" + data['data'];
            document.getElementById('person3').src = re.img_3 = imagePath;
            gtag('event', "click", {
                event_category: "result",
                event_label: "addphoto"
            });

            gtag('event', "click", {
                event_category: "result",
                event_label: "add3"
            });
        }
    })

}
function load4() {
    var uploader = new plupload.Uploader({
        runtimes: 'html5, html4',
        browse_button: 'person4', //触发文件选择对话框的按钮，为那个元素id
        url: '/index.php?c=index&f=upload', //服务器端的上传页面地址
        flash_swf_url: '/src/js/plupload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        filters: {
            mime_types: [
                {title: "Image files", extensions: "jpg,gif,png,bmp"}
            ],
            max_file_size: '10mb'
        }, //图片限制
        silverlight_xap_url: 'js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        init: {
            FilesAdded: function (uploader, files) {
                uploader.start()
            }
        }
    });

    uploader.init();
    uploader.bind('FilesAdded', function (uploader, files) {
        uploader.start();
    });
    uploader.bind('Error', pluploadError);
    uploader.bind('UploadProgress', function (uploader, file) {
        console.log(file.percent)
        re.uploadingFlag = 1
        re.uploadPercent = file.percent
    })

    uploader.bind('FileUploaded', function (uploader, file, responseObject) {
        var data = JSON.parse(responseObject.response);
        re.uploadingFlag = 0

        if (data && data['code'] == "200") {
            var imagePath = "/" + data['data'];
            document.getElementById('person4').src = re.img_4= imagePath;
            gtag('event', "click", {
                event_category: "result",
                event_label: "addphoto"
            });

            gtag('event', "click", {
                event_category: "result",
                event_label: "add4"
            });
        }
    })

}
function load5() {
    var uploader = new plupload.Uploader({
        runtimes: 'html5, html4',
        browse_button: 'person5', //触发文件选择对话框的按钮，为那个元素id
        url: '/index.php?c=index&f=upload', //服务器端的上传页面地址
        flash_swf_url: '/src/js/plupload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        filters: {
            mime_types: [
                {title: "Image files", extensions: "jpg,gif,png,bmp"}
            ],
            max_file_size: '10mb'
        }, //图片限制
        silverlight_xap_url: 'js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        init: {
            FilesAdded: function (uploader, files) {
                uploader.start()
            }
        }
    });

    uploader.init();
    uploader.bind('Error', pluploadError);
    uploader.bind('FilesAdded', function (uploader, files) {
        uploader.start();
    });
    uploader.bind('UploadProgress', function (uploader, file) {
        console.log(file.percent)
        re.uploadingFlag = 1
        re.uploadPercent = file.percent
    })

    uploader.bind('FileUploaded', function (uploader, file, responseObject) {
        var data = JSON.parse(responseObject.response);
        re.uploadingFlag = 0

        if (data && data['code'] == "200") {
            var imagePath = "/" + data['data'];
            document.getElementById('person5').src = re.img_5 = imagePath;
            gtag('event', "click", {
                event_category: "result",
                event_label: "addphoto"
            });

            gtag('event', "click", {
                event_category: "result",
                event_label: "add5"
            });
        }
    })

}
function load6() {
    var uploader = new plupload.Uploader({
        runtimes: 'html5, html4',
        browse_button: 'person6', //触发文件选择对话框的按钮，为那个元素id
        url: '/index.php?c=index&f=upload', //服务器端的上传页面地址
        flash_swf_url: '/src/js/plupload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        filters: {
            mime_types: [
                {title: "Image files", extensions: "jpg,gif,png,bmp"}
            ],
            max_file_size: '10mb'
        }, //图片限制
        silverlight_xap_url: 'js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        init: {
            FilesAdded: function (uploader, files) {
                uploader.start()
            }
        }
    });

    uploader.init();
    uploader.bind('Error', pluploadError);
    uploader.bind('FilesAdded', function (uploader, files) {
        uploader.start();
    });
    uploader.bind('UploadProgress', function (uploader, file) {
        console.log(file.percent)
        re.uploadingFlag = 1
        re.uploadPercent = file.percent
    })

    uploader.bind('FileUploaded', function (uploader, file, responseObject) {
        var data = JSON.parse(responseObject.response);
        re.uploadingFlag = 0

        if (data && data['code'] == "200") {
            var imagePath = "/" + data['data'];
            document.getElementById('person6').src = re.img_6= imagePath;
            gtag('event', "click", {
                event_category: "result",
                event_label: "addphoto"
            });

            gtag('event', "click", {
                event_category: "result",
                event_label: "add6"
            });
        }
    })

}
function init() {
    document.getElementById('full_name_id').disabled=true;
    document.getElementById('email_id').disabled=true;
    document.getElementById('phone_number_id').disabled=true;
    document.getElementById('birthdate_id').disabled=true;
    document.getElementById('industry_id').disabled=true;
    document.getElementById('hometown_id').disabled=true;
    document.getElementById('personality_id').disabled=true;
    document.getElementById('constellation_id').disabled=true;
}

window.onload=init();




