window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-92633598-19', {
    'custom_map': {'dimension1': 'entry'}
});