vm=new Vue({
    el:"#app",
    data:{
        success_style:{
            success_class_a:true,
            success_class_b:false
        },
        sign_up:"<p>Sign up to become a UC Miss Cricket. </p>",
        src_btn:"",
        logo_btn:"/src/images/logo.png",
        isStateShow:0,
        isRuleShow: 0
    },
    methods: {
        signUp: function () {
            gtag('event', "click", {
                event_category: "home",
                event_label: "signup"
            });
            var url = document.getElementById("data-btn-url").value;
            window.location.href = url;
        },
        rules: function () {
            gtag('event', "click", {
                event_category: "home",
                event_label: "rule"
            });

            this.isRuleShow = 1
        },
        share: function () {
            //分享链接
            gtag('event', "click", {
                event_category: "home",
                event_label: "clientshare"
            });
            window.location.href = "https://m.facebook.com/dialog/feed?app_id=156971551678798&redirect_uri=http%3a%2f%2fucmisscricket.2.xuandev.com%2f&link=http%3a%2f%2fucmisscricket.2.xuandev.com%2f"
        },
        GetQueryString: function (name) {
            reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)');
            r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return unescape(r[2]);
            }
        }
    },


    created: function () {
        // Sends an event that passes 'age' as a parameter.
        gtag('event', 'entry_dimension', {'entry': document.getElementById("data-entry").value});

        //facebook登录
        FB.init({
            appId: '156971551678798',
            status: true,
            cookie: true,
            xfbml: true,
            oauth: true
        });

        var facebookLoginCode;
        facebookLoginCode = this.GetQueryString("code");

        if (facebookLoginCode) {
            FB.getLoginStatus(function (response) {
                var accessToken, uid;
                if (response.status === 'connected') {
                    uid = response.authResponse.userID;
                    accessToken = response.authResponse.accessToken;
                    FB.api('/me', {fields: 'name'}, function (response2) {
                        return $.post("/index.php?c=index&f=login", {
                            id: uid,
                            token: accessToken,
                            name: response2.name
                        }, function (res) {
                            if (res.code === "200") {
                                return window.location.href = "/index.php?c=index&f=result";
                            }
                        }, "json");
                    });

                } else if (response.status === 'not_authorized') {
                    return alert('Auth Facebook First');
                }
            });
        }

        var uid = document.getElementById("data-uid").value;
        var state=document.getElementById("data-state").value;
        // if(state==3){
        //     this.isStateShow=1;
        //     document.getElementById("state_tip").innerHTML="Sorry!You fail to sign up .</br> Please check your profile";
        //     setTimeout(function () {
        //         vm.isStateShow=0;
        //     },5000)
        // }else if(state==4){
        //     this.isStateShow=1;
        //     document.getElementById("state_tip").innerHTML="Congratulations!<br> Sign up successfully";
        //     setTimeout(function () {
        //         vm.isStateShow=0;
        //     },5000)
        // }else if(state==1){
        //     this.isStateShow=1;
        //     document.getElementById("state_tip").innerHTML="Please be patient.<br> The information is under review";
        //     setTimeout(function () {
        //         vm.isStateShow=0;
        //     },5000)
        // }

        this.isStateShow=1;
        document.getElementById("state_tip").innerHTML='"""T20 2018 · UC Miss Cricket"" campaign is under way,<br />' +
            'Now go and vote for your favorite UC Miss Cricket！"';
        setTimeout(function () {
            vm.isStateShow=0;
        },5000)


        if (uid !== "") {
            this.sign_up = "<p>UC Miss Cricket is coming soon...</p>";
            this.src_btn = "/src/images/check.png";
            setTimeout(function () {
                vm.isshow = !vm.isshow
            }, 5000)
        } else {
            this.sign_up = "<p>Sign up to become a UC Miss Cricket. </p>";
            this.src_btn = "/src/images/sign-up_btn.png";
            this.isshow = !this.isshow;
        }
    }
});

