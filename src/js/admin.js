vm = new Vue({
    el: ".user-list",
    data: {
        userList: [{
            ctime: "",
            image: {
                img_1: "",
                img_2: "",
                img_3: "",
                img_4: "",
                img_5: "",
                img_6: "",
            }
        }],
        pageData: {
            total: 0,
            page: 1,
            totalPage: 1
        },
        searchdata: {
            fullname: "",
            industry: "",
            hometown: "",
            state: '',
            facebookname: '',
            age: "",
            personality: "",
            constellation: "",
            platform: "",
            page: 1
        },
        pageList: [],
        newUrl: "",
        hoverFlag: -1,
        hoverImagePath: '',
        industrys:[
            "Student","Art","Entertainment","Finance & economic","Healthcare & Medicine","Industrial & Manufacturing","IT & Internet","Media","Retail","Science & Technology","Other"
        ],
        personalitys:[
            "Charming","Active","Athletic","Creative","Friendly","Independent","Kind","Passionate",
            "Popular","Romantic","Sexy","Sweet","Modern","Passionate","Loveable","Intelligent"
        ],
        hometowns:[
            "Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Jharkhand","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Sikkim","Tripura","West Bengal","Delhi","Haryana","Himachal Pradesh","Jammu & Kashmir","Punjab","Rajasthan","Uttar Pradesh","Uttarakhand","Andhra Pradesh","Karnataka","Kerala","Tamil Nadu","Telangana","Goa","Gujarat","Madhya Pradesh","Maharashtra"
        ],
        lineId: []
    },
    watch: {
        pageData: {
            deep: true,
            handler: function() {
                console.log(this.pageData)
                var i = this.searchdata.page
                var tmp = []
                while(i < this.pageData.totalPage && i < this.searchdata.page + 10) {
                    tmp.push(i)
                    i++
                }

                this.pageList = tmp
            }
        },
        searchdata: {
            deep: true,
            handler: function() {
                this.getApplyList()
            }
        }
    },
    methods: {
        importInit: function() {
            var uploader = new plupload.Uploader({
                runtimes: 'html5, html4',
                browse_button: 'upload', //触发文件选择对话框的按钮，为那个元素id
                url: '/index.php?c=index&f=upload', //服务器端的上传页面地址
                flash_swf_url: '/src/js/plupload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
                filters: {
                    mime_types: [
                        {title: "Image files", extensions: "xls,xlsx"}
                    ],
                    max_file_size: '10mb'
                }, //图片限制
                silverlight_xap_url: 'js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
                init: {
                    FilesAdded: function (uploader, files) {
                        uploader.start()
                    }
                }
            });

            uploader.init();
            uploader.bind('FilesAdded', function (uploader, files) {
                uploader.start();
            });

            uploader.bind('FileUploaded', function (uploader, file, responseObject) {
                var data = JSON.parse(responseObject.response);

                if (data && data['code'] == "200") {
                    alert('upload successful. importing. please refresh the window for a while later')
                } else {
                    alert(data.message)
                }
            })
        },
        changeState: function(state, item, index) {
            this.$http.post(
                    "/index.php?c=index&f=change_state",
                    {
                        state: state,
                        id: item.uid
                    },
                    {
                        emulateJSON: true
                    }
            ).then(function(res) {
                this.userList.splice(index, 1)
            })
        },
        getApplyList: function() {
            this.$http.post(
                    "/index.php?c=index&f=get_data_list",
                    {
                        param: this.searchdata
                    },
                    {
                        emulateJSON: true
                    }
            ).then(function(res) {
                // console.log(JSON.parse(res.body)['data'])
                var dataList = JSON.parse(res.body)['data']
                var i = 0
                for(i = 0; i < dataList.length; i++) {
                    if (dataList[i]['image']) {
                        dataList[i]['image'] = JSON.parse(dataList[i]['image'])
                    }
                }
                this.userList = dataList
                this.pageData = JSON.parse(res.body)['page']
                // this.userList = res['data']
            })
        },
        changeLineId: function(uid) {
            index = this.lineId.indexOf(uid)
            if (index >= 0) {
                this.lineId.splice(index, 1)
            } else {
                this.lineId.push(uid)
            }
        },
        batchSubmit: function(state) {
            for(index in this.lineId) {
                console.log(index, this.lineId[index])
                this.changeState(state, {uid: this.lineId[index]}, -1);
            }

            alert("处理完成，请手动刷新")
        },
        getNewUrl: function() {
            this.$http.post(
                    "/index.php?c=index&f=get_new_url",
                    {
                        emulateJSON: true
                    }
            ).then(function(res) {
                this.newUrl = res.body
            })
        },
        submitUrl: function() {
            this.$http.post(
                    "/index.php?c=index&f=submit_url",
                    {
                        url: this.newUrl
                    },
                    {
                        emulateJSON: true
                    }
            ).then(function(res) {
                alert("success")
                window.location.reload()
            })
        }
    },
    mounted: function() {
        this.getApplyList()
        this.getNewUrl()
        this.importInit()
    }
});