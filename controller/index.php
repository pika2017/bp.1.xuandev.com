<?php

class index extends engine {
    public function home() {
        $smarty = new Smarty();
        
        $quizType = mt_rand(1, 3);

        $shareTitle = [
            "Want to Win Rs. 10 Lakhs & Be Famous? Be Miss UC Cricket Like Me!",
            "I've Just Signed to be Miss UC Cricket! Join Me & Win Rs. 10 Lakhs!",
            "Only the Beauties Can be Miss UC Cricket! I've Signed up. Will You?!",
        ];

        $index = mt_rand(0, 2);
        $smarty->assign("tilte", $shareTitle[$index]);

        ##储存风格
        session_start();

        if ($_SESSION['uid']) {

            $data = M("user")->getData($_SESSION['uid'])['data'];
            $smarty->assign("state", $data['status']);

            $smarty->assign("uid", $_SESSION['uid']);

            if ($data['status'] != 4) {
                $smarty->assign("url", '');
            } else {
                $smarty->assign("url", "http://" . $_SERVER['HTTP_HOST'] . "/index.php?c=index&f=result");
            }

        } else {
            $smarty->assign("uid", '');
            $smarty->assign("url", "https://m.facebook.com/dialog/oauth?client_id=156971551678798&redirect_uri=http://" . $_SERVER['HTTP_HOST'] . "&scope=email");
        }

        if (isset($_GET['entry'])) {
            setcookie("entry", $_GET['entry']);
        }

        $smarty->display("index.html");
    }

    public function test1() {
        $newUrl = M("user")->getNewUrl();
    }

    public function get_new_url() {
        $newUrl = M("user")->getNewUrl();
        echo $newUrl;
    }

    public function submit_url() {
        $url = $_POST['url'];
        $result = M("user")->setNewUrl($url);
        echo $result;
    }

    public function result() {
        session_start();
        $uid = $_SESSION['uid'];

        if ($uid) {

            $smarty = new Smarty();
            $smarty->assign("nickname", $_SESSION['name']);
            $smarty->display("result.html");
        } else {
            header("location: /");
        }

    }

    public function login() {
        $userId = $_POST['id'];
        $token = $_POST['token'];
        $name = $_POST['name'];

        $result = M("user")->login($userId, $token, $name);

        echo json_encode($result);
    }

    public function submit_data() {
        $data = $_POST['data'];

        $result = M("user")->submitData($data);

        echo json_encode($result);
    }

    public function import($pathname) {
        require_once ("engine/plugins/PHPExcel.class.php");
        require_once ("engine/plugins/PHPExcel/Writer/Excel5.php");
        require_once ("engine/plugins/PHPExcel/IOFactory.php");

//        $pathname  = "upload/15224520941162866.xls";

        $objPHPExcel = new \PHPExcel();
        $objProps = $objPHPExcel->getProperties();

        //创建PHPExcel对象，注意，不能少了\
        $reader = \PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $reader->load($pathname); // 载入excel文件
        $objActSheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objActSheet->getHighestRow();
        $highestColumm = $objActSheet->getHighestColumn(); // 取得总列数


        $dataList = array();
        for ($row = 1; $row <= $highestRow; $row++) {

            $dataList[$row] = array();
            for ($column = 'A'; $column <= $highestColumm; $column++) {
                $dataList[$row][$column] = $objActSheet->getCell($column . $row)->getValue();
                //$tmp = $objActSheet->getCell($column . $row)->getValue();
            }
        }

//        var_dump($dataList);

        foreach ($dataList as $key => $value) {
            if ($value['S'] and is_numeric($value['S'])) {//判断 openid 是否为空
                $link = D();

                $imageList = array();
                if ($value['B']) {
                    $imageList['img_1'] = $value['B'];
                }

                if ($value['C']) {
                    $imageList['img_2'] = $value['C'];
                }

                if ($value['D']) {
                    $imageList['img_3'] = $value['D'];
                }

                if ($value['E']) {
                    $imageList['img_4'] = $value['F'];
                }

                if ($value['F']) {
                    $imageList['img_5'] = $value['G'];
                }

                $status = '';
                if ($value['Q'] == 'rejected') {
                    $status = 3;
                } else if ($value['Q'] == 'passed') {
                    $status = 4;
                } else if ($value['Q'] == 'under review') {
                    $status = 1;
                } else if ($value['Q'] == 'signuponly') {
                    $status = 2;
                }

                #查询用户是否存在
                $link->query("UPDATE bq_user SET 
                about = '" . $value['H'] . "' ,
                hometown = '" . $value['N'] . "' ,
                birthday = '" . $value['L'] . "' ,
                phone = '" . $value['K'] . "" . "' ,
                personlity = '" . $value['P'] . "' ,
                constellation = '" . $value['O'] . "' ,
                name = '" . $value['I'] . "' ,
                email = '" . $value['J'] . "' ,
                industry = '" . $value['M'] . "' ,
                url = '" . $value['U'] . "' ,
                image = '" . json_encode($imageList) . "' ,
                status = " . $status . " WHERE openid = '" . $value['S'] . "'");
            }
        }
    }

    public function upload() {
        $file = $_FILES['file'];//得到传输的数据
        //得到文件名称
        $name = $file['name'];
        $type = strtolower(substr($name,strrpos($name,'.') + 1)); //得到文件类型，并且都转化成小写
        $allow_type = array('jpg','jpeg','gif','png', 'bmp', 'xls', 'xlsx'); //定义允许上传的类型

        //判断文件类型是否被允许上传
        if(!in_array($type, $allow_type)){
            //如果不被允许，则直接停止程序运行
            echo json_encode(array(
                "code" => "400",
                "message" => "you must upload an image"
            ));
            exit;
        }

        if ($file['size'] > 10240 * 1000) {
            echo json_encode(array(
                "code" => "400",
                "message" => "You need to upload the image size less than 10m"
            ));
            exit;
        }

        //判断是否是通过HTTP POST上传的
        if(!is_uploaded_file($file['tmp_name'])){
            //如果不是通过HTTP POST上传的
            return ;
        }

        $upload_path = "upload/"; //上传文件的存放路径

        if (!is_dir($upload_path)) {
            mkdir("upload", 0775);
        }

        //开始移动文件到相应的文件夹
        $rename = time() . mt_rand(1000000, 2000000) . "." . $type;
        if(move_uploaded_file($file['tmp_name'], $upload_path . $rename)){
            setcookie("upload_image", $upload_path . $rename);

            //导入 excel
            if (in_array($type, array('xlsx', 'xls'))) {
                $this->import($upload_path . $rename);
            }

            echo json_encode(array(
                "code" => "200",
                "data" => $upload_path . $rename
            ));
        }else{
            echo json_encode(array(
                "code" => "400",
                "message" => "upload failed"
            ));
        }
    }

    public function admin() {
        $pwd = $_GET['pwd'];

        if ($pwd == "12345678") {

            $smarty = new Smarty();

            $smarty->display("admin.html");
        }
    }


    public function get_data_list() {
        $param = $_POST['param'];
        $result = M("user")->getDataList($param);

        echo json_encode($result);
    }

    public function get_data() {
        session_start();
        $uid = $_SESSION['uid'];

        if ($uid) {
            $result = M("user")->getData($uid);

            echo json_encode($result);
        }
    }

    public function change_state() {
        $state = $_POST['state'];
        $uid = $_POST['id'];

        $result = M("user")->changeState($state, $uid);
        echo json_encode($result);
    }

    public function download() {
        require_once ("engine/plugins/PHPExcel.class.php");
        require_once ("engine/plugins/PHPExcel/Writer/Excel5.php");
        require_once ("engine/plugins/PHPExcel/IOFactory.php");


        $fileName = date("Y-m-d H-i-s") . ".xls";
        $headArr = array("Time","pic1","pic2","pic3","pic4", "pic5","pic6","about me","full name","email","phone", "age", "industry","hometown"
        ,"constellation", "personality", "state", "enter address", 'facebook openid', 'facebook name');

        //创建PHPExcel对象，注意，不能少了\
        $objPHPExcel = new \PHPExcel();

        //设置表头
        $key = ord("A");
        foreach($headArr as $v) {
            $colum = chr($key);
            $objPHPExcel->setActiveSheetIndex(0) ->setCellValue($colum.'1', $v);
            $key += 1;
        }

        $state = $_GET['state'] ? : '';
        $param = array();
        if ($state) {
            $param['state'] = $state;
        }

        $param['unlimited'] = 1;

        $data = M("user")->getDataList($param);

        $column = 2;

//        var_dump($data['data']);
//        exit;
        $objActSheet = $objPHPExcel->getActiveSheet();
        foreach($data['data'] as $key => $rows) { //行写入
            $span = ord("A");
            foreach($headArr as $keyName => $value){// 列写入
                $j = chr($span);

                if ($j == "A") {
                    $value = $rows['ctime'];
                }

                if ($j == "B") {
                    $image = json_decode($rows['image'], true);
                    $value = $image['img_1'] != '/src/images/add_img_1.png' ? $image['img_1'] : '';
                    if ($value) {
                        $value = "http://" . $_SERVER['HTTP_HOST'] . $value;
                    }
                }

                if ($j == "C") {
                    $image = json_decode($rows['image'], true);
                    $value = $image['img_2'] != '/src/images/add_img_2.png' ? $image['img_2'] : '';
                    if ($value) {
                        $value = "http://" . $_SERVER['HTTP_HOST'] . $value;
                    }
                }

                if ($j == "D") {
                    $image = json_decode($rows['image'], true);
                    $value = $image['img_3'] != '/src/images/add_img_3.png' ? $image['img_3'] : '';
                    if ($value) {
                        $value = "http://" . $_SERVER['HTTP_HOST'] . $value;
                    }
                }

                if ($j == "E") {
                    $image = json_decode($rows['image'], true);
                    $value = $image['img_4'] != '/src/images/add_img_4.png' ? $image['img_4'] : '';
                    if ($value) {
                        $value = "http://" . $_SERVER['HTTP_HOST'] . $value;
                    }
                }

                if ($j == "F") {
                    $image = json_decode($rows['image'], true);
                    $value = $image['img_5'] != '/src/images/add_img_5.png' ? $image['img_5'] : '';
                    if ($value) {
                        $value = "http://" . $_SERVER['HTTP_HOST'] . $value;
                    }
                }

                if ($j == "G") {
                    $image = json_decode($rows['image'], true);
                    $value = $image['img_6'] != '/src/images/add_img_6.png' ? $image['img_6'] : '';
                    if ($value) {
                        $value = "http://" . $_SERVER['HTTP_HOST'] . $value;
                    }
                }

                if ($j == "H") {
                    $value =  iconv('GB2312', 'UTF-8', $rows['about']);
                }

                if ($j == "I") {
                    $value = $rows['name'];
                }

                if ($j == "J") {
                    $value = $rows['email'];
                }

                if ($j == "K") {
                    $value = $rows['phone'] + ' ';
                }

                if ($j == "L") {
                    $value = $rows['birthday'];
                }

                if ($j == "M") {
                    $value = $rows['industry'];
                }

                if ($j == "N") {
                    $value = $rows['hometown'];
                }

                if ($j == "O") {
                    $value = $rows['constellation'];
                }

                if ($j == "P") {
                    $value = $rows['personlity'];
                }

                if ($j == "Q") {
                    if ($rows['status'] == 1) {
                        $value = "under review";
                    } else if ($rows['status'] == 4) {
                        $value = "passed";
                    } else if ($rows['status'] == 2) {
                        $value = "signuponly";
                    } else if ($rows['status'] == 3) {
                        $value = 'rejected';
                    }
                }

                if ($j == 'R') {
                    $value = $rows['platform'];
//                    var_dump($value);
                }

                if ($j == 'S') {
                    $value = $rows['openid'];
//                    var_dump($value);
                }

                if ($j == 'T') {
                    $value = $rows['nickname'];
//                    var_dump($value);
                }

                $objActSheet->setCellValue($j.$column, $value);
                $span++;
            }
            $column++;
        }

        $fileName = iconv("utf-8", "gb2312", $fileName);
        //重命名表
        $objPHPExcel->getActiveSheet()->setTitle('test');
        //设置活动单指数到第一个表,所以Excel打开这是第一个表
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=\"$fileName\"");
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output'); //文件通过浏览器下载

    }
}