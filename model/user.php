<?php


class user extends engine {
    public function login($facebookId, $token, $name) {
        if ($facebookId) {

            ##实例化数据库
            $link = D();

            #查询用户是否存在
            $result = $link->query("SELECT uid, nickname FROM bq_user WHERE openid = '$facebookId' LIMIT 0, 1");

            $row = mysqli_fetch_assoc($result);

            if ($row['uid']) {

                //自动登录，记录session和cookie
                SetCookie("uid", $row['uid'],time() + 3600 * 24 * 7, "/");

                session_start();

                $_SESSION["uid"] = $row['uid'];
                $_SESSION["name"] = $row['nickname'];

            } else {
                $ctime = date("Y-m-d H:i:s", time());
                $platform = $_COOKIE['entry'] ? : '';
                $link->query("INSERT INTO bq_user (`openid`, `nickname`, `ctime`, `platform`) VALUES ('$facebookId', '$name', '$ctime', '$platform')");

                $result = $link->query("SELECT uid, nickname FROM bq_user WHERE openid = '$facebookId' LIMIT 0, 1");

                $row = mysqli_fetch_assoc($result);

                SetCookie("uid", $row['uid'],time() + 3600 * 24 * 7, "/");

                session_start();

                $_SESSION["uid"] = $row['uid'];
                $_SESSION["name"] = $row['nickname'];
            }

            return array(
                "code" => "200",
                "message" => "success"
            );
        } else {
            return array(
                "code" => "400",
                "message" => "failed"
            );
        }
    }

    public function submitData($data) {
        session_start();
        $uid = $_SESSION['uid'];
        if ($uid) {

            if (!$data['industry']) {
                return array(
                    "code" => "400",
                    "data" => $data,
                    "message" => "industry empty"
                );
            }

            if (!$data['email']) {
                return array(
                    "code" => "400",
                    "message" => "email empty"
                );
            }

            if (!$data['name']) {
                return array(
                    "code" => "400",
                    "message" => "fullname empty"
                );
            }

            if (!$data['personality']) {
                return array(
                    "code" => "400",
                    "message" => "personlity empty"
                );
            }

            if (!$data['phone']) {
                return array(
                    "code" => "400",
                    "message" => "phone empty"
                );
            }

            if (!$data['birthdate']) {
                return array(
                    "code" => "400",
                    "message" => "age empty"
                );
            }

            if (!$data['hometown']) {
                return array(
                    "code" => "400",
                    "message" => "hometown empty"
                );
            }

            if (!$data['about']) {
                return array(
                    "code" => "400",
                    "message" => "about empty"
                );
            }

            ##实例化数据库
            $link = D();

            #查询用户是否存在
            $link->query("UPDATE bq_user SET 
                about = '" . $data['about'] . "' ,
                hometown = '" . $data['hometown'] . "' ,
                birthday = '" . $data['birthdate'] . "' ,
                phone = '" . $data['phone'] . "' ,
                personlity = '" . $data['personality'] . "' ,
                constellation = '" . $data['constellation'] . "' ,
                name = '" . $data['name'] . "' ,
                email = '" . $data['email'] . "' ,
                industry = '" . $data['industry'] . "' ,
                image = '" . json_encode($data['image']) . "' ,
                status = 1 WHERE uid = $uid");

            return array(
                "code" => "200",
                "message" => "提交成功"
            );
        } else {
            return array(
                "code" => "400",
                "message" => "请先登录"
            );
        }
    }

    public function getData($uid) {
        ##实例化数据库
        $link = D();

        #查询用户是否存在
        $result = $link->query("SELECT * FROM bq_user WHERE uid = '$uid' LIMIT 0, 1");

        $row = mysqli_fetch_assoc($result);

        if ($row['uid']) {
            return array(
                "code" => "200",
                "data" => $row
            );
        } else {
            return array(
                "code" => "400",
                "message" => "数据不存在"
            );
        }
    }

    public function getDataList($param) {
        $link = D();

        if ($param['state']) {
            $where = "status = " . $param['state'];
        }

        if ($param['fullname']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'name like "%' . $param['fullname'] . '%"';
        }

        if ($param['facebookname']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'nickname like "%' . $param['facebookname'] . '%"';
        }

        if ($param['age']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'birthday = "' . $param['age'] . '"';
        }

        if ($param['industry']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'industry = "' . $param['industry'] . '"';
        }

        if ($param['hometown']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'hometown = "' . $param['hometown'] . '"';
        }

        if ($param['constellation']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'constellation = "' . $param['constellation'] . '"';
        }

        if ($param['personality']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'personlity = "' . $param['personality'] . '"';
        }

        if ($param['platform']) {
            if ($where) {
                $where .= " and ";
            }

            $where .= 'platform like "%' . $param['platform'] . '%"';
        }

        if (!$where) {
            $where = 1;
        }

        $page = intval($param['page']) ? : 1;
        $perPage = 30;
        $startLimit = ($page - 1) * $perPage;
        $pageStr = "";

        if (!$param['unlimited']) {
            $pageStr =  " limit " . $startLimit . ", " . $perPage;
        }

        #查询用户是否存在
        $resultPage = $link->query("SELECT count(*) as total FROM bq_user where " . $where);
        $rowPage = mysqli_fetch_assoc($resultPage)['total'];
        $pageData['total'] = $rowPage;
        $pageData['totalPage'] = ceil($rowPage / $perPage);

        $result = $link->query("SELECT * FROM bq_user where " . $where . " order by ctime desc" . $pageStr);

        $data = array();
        while($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }

        return array(
            "code" => "200",
            "data" => $data,
            "page" => $pageData
        );
    }

    public function getNewUrl() {
        $link = D();
        $result = $link->query("SELECT * FROM bq_config where type = 'LINK_URL'");
        $row = mysqli_fetch_assoc($result);
        return $row['content'];
    }

    public function setNewUrl($url) {
        $link = D();
        $result = $link->query("UPDATE bq_config SET content = '" . $url . "' where type = 'LINK_URL'" );
        return $url;

    }

    public function changeState($state, $uid) {
        ##实例化数据库
        $link = D();

        #查询用户是否存在
        $result = $link->query("UPDATE bq_user SET status = " . $state . " where uid = " . $uid);

        return 1;
    }
}